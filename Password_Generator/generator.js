// DOM elements

const resultEl = document.getElementById('result');
const lengthEl = document.getElementById('length');
const uppercaseEl = document.getElementById('uppercase');
const lowercaseEl = document.getElementById('lowercase');
const numbersEl = document.getElementById('numbers');
const symbolsEl = document.getElementById('symbols');
const generateEl = document.getElementById('generate');
const clipboardEl = document.getElementById('clipboard');


const randomFunction = {
    upper : getRandomUpper,
    lower : getRandomLower,
    number : getRandomNumber,
    symbol : getRandomSymbol
};

// Get values to make the passowrd

generateEl.addEventListener('click', () => {
    //Size of the password
    const length = +lengthEl.value;

    //Options of the password
    const hasUpper = uppercaseEl.checked;
    const hasLower = lowercaseEl.checked;
    const hasNumber = numbersEl.checked;
    const hasSymbol = symbolsEl.checked;

    resultEl.innerText = generatePassword(
        hasUpper,
        hasLower,
        hasNumber,
        hasSymbol,
        length)
});

// Copy password to clipboard
clipboardEl.addEventListener('click', () => {
    const textarea = document.createElement('textarea');
    const password = resultEl.innerText;

    if(!password){
        return;
    }

    textarea.value = password;
    document.body.appendChild(textarea);
    textarea.select();
    document.execCommand('copy');
    textarea.remove();
    alert('Mot de passe copié dans le presse-papier !');
})

// Generate password

function generatePassword(upper, lower, number, symbol, length) {
    
    // Road map of the function

    // 1) Init password variable
    // 2) Filter unchecked types of options
    // 3) Loop over length call generator function for each option
    // 4) Add final password to the password variable

    let generatedPassword = '';
    
    const typesCount = upper + lower + number + symbol
    const typesArray = [{ upper }, { lower }, { number }, { symbol }].filter
    (
        item => Object.values(item)[0]
    );

    if (typesCount === 0) {
        return '';
    }

    // Traversy Media's way
    
    // for(let i = 0; i < length; i += typesCount) {
    //     typesArray.forEach(type => {
    //         const funcName = Object.keys(type)[0];
    //         generatedPassword += randomFunction[funcName]();
    //     })
    // }
    // const finalPassword = generatedPassword.slice(0, length);



    // From a comment under the video (from awaterplease)
    // Shuffle the types at each turn of the loop
    for(let i = 0; i < length; i++){
        const rand = Math.floor(Math.random() * typesArray.length)
        generatedPassword += randomFunction[Object.keys(typesArray[rand])[0]]();
    }
    console.log(generatedPassword)
    const finalPassword = generatedPassword;

    return finalPassword;
}

// Generator funcitons

function getRandomLower() {
    return String.fromCharCode(Math.floor(Math.random() * 26) + 97);
}

function getRandomUpper() {
    return String.fromCharCode(Math.floor(Math.random() * 26) + 65);
}

function getRandomNumber() {
    return String.fromCharCode(Math.floor(Math.random() * 10) + 48);
}

function getRandomSymbol() {
    const symbols = "&~#{([|@°)]}£$¤%*<>?!§^"
    return symbols[Math.floor(Math.random()* symbols.length)];
}