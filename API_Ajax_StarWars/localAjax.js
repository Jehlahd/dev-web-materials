document.getElementById('collectData').addEventListener('click', loadData);

function loadData(){
    // Create XHR Object
    var xhr = new XMLHttpRequest();
    console.log(xhr);
    var data = 'data.json'
    xhr.open('GET',data , true);

    xhr.onload = function (){
        if(this.status == 200){
            var datas = JSON.parse(this.responseText)
            console.log(datas.hobbies[1])

            var output = "";

                output += '<ul>' + 
                '<li>Nom : '+datas.name+'</li>' +
                '</ul>'; 
            document.getElementById('data').innerHTML = output;
        }
    }

    xhr.onerror = function() {
        console.log ('Request Error...')
    }

    /*
    Deprecated way :

    xhr.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            console.log(this.responseText)
        }
    }

    */

    xhr.send();
}