document.getElementById('collectData').addEventListener('click', loadData);

function loadData(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://swapi.dev/api/people', true);

    xhr.onload = function() {
        if(this.status == 200) {
            var peoples = JSON.parse(this.responseText);

            var output = '';
            var people = peoples.results;
            console.log(people);

            for(var i = 0; i < people.length; i++){
                    output +=
                    '<hr>'+
                    '<p class="character'+i+'">Personnage n°'+i+'</p>' +
                    '<p class="character'+i+'">Nom : ' + people[i].name + '</p>' +
                    '<p class="character'+i+'">Date de naissance : ' + people[i].birth_year + '</p>' +
                    '<p class="character'+i+'">Genre : ' + people[i].gender + '</p>' +
                    '<p class="character'+i+'">Taille : ' + people[i].height + '</p>' +
                    '<p class="character'+i+'">Poids : ' + people[i].mass + '</p>' +
                    '<p class="character'+i+'">Cheveux(couleur) : ' + people[i].hair_color + '</p>' +
                    '<p class="character'+i+'">Yeux(couleur) : ' + people[i].eye_color + '</p>' +
                    '<p class="character'+i+'">Peau(couleur) : ' + people[i].skin_color + '</p>' +
                    '<hr>'
            }
            document.getElementById('data').innerHTML = output;
        }
    }

    xhr.send();
}