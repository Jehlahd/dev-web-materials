const express = require('express');
const User = require ('../database/user')
const router = express.Router();

const user = new User();

// Get index page
router.get('/', (req, res, next) => {
    res.render('index', {title:"Système de connexion + enregistrement sécurisé"});
});

// Get home page
router.get('/home', (req, res, next) => {
    res.send('C\'est la page d\'accueil !')
});

// Post login data
router.post('/login', (req, res, next) => {
    
    user.login(req.body.username, req.body.password, function(result){
        if(result){
            res.send('Connecté.e en tant que : '+result.username)
        } else {
            res.send('Nom d\'utilisateur / Mot de passe invalide !')
        }
    })
})

// Post register data
router.post('/register', (req, res, next) => {
    
    let userInput = {
        username : req.body.username,
        email : req.body.email,
        password : req.body.password
    };

    user.create(userInput, function(lastId) {
        if(lastId) {
            console.log(userInput);
            res.send('Bienvenue '+userInput.username+' !')
        } else {
            console.log(userInput)
            console.log('Error creating a new user...')
        }
    })
})

module.exports = router;