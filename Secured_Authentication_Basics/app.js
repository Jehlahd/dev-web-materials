const express = require('express');
const path = require('path');
const pageRouter = require('./routes/pages');
const app = express();

// For body-parser
app.use(express.urlencoded( { extended : false}));

// Static files
app.use(express.static(path.join(__dirname, 'public')));

// Set template engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Set up routers
app.use('/', pageRouter);

// ----- Listing errors -----
// Error 404 : Page not found
app.use((req, res, next) => {
    var err = new Error('Page not found');
    err.status = 404;
    next(err);
});

// Handling errors
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send(err.message);
})

// Listening the server
app.listen(3000, () => {
    console.log('Server running on port 3000');
});

module.exports = app;