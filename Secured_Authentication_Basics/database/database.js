const mysql = require('mysql');
const util = require('util');
/**
 * Connection to the database
 */
const db = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'web_dev_materials'
});

db.getConnection((err, connection) => {
    if(err) {
        console.error("Can't connect to the database.")
    }
    if(connection) {
        connection.release();
    }
    return;
});

db.query = util.promisify(db.query);

module.exports = db;