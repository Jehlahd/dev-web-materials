const database = require("./database");
// Use bcrypt to has and secure passwords
const bcrypt = require("bcrypt");

// Create a User class to send queries to the database
function User() {}

User.prototype = {
  find: function (user = null, callback) {
    if (user) {
      var field = Number.isInteger(user) ? "id" : "username";
    }

    let sql = `SELECT * FROM security_login_basics WHERE ${field} = ?`;

    database.query(sql, user, function (err, result) {
      if (err) throw err;

      if (result.length) {
        callback(result[0]);
      } else {
        callback(null);
      }
    });
  },

  create: function (body, callback) {
    let pwd = body.password;
    body.password = bcrypt.hashSync(pwd, 10);

    let bind = {};
    bind.username = body.username;
    bind.email = body.email;
    bind.password = body.password;

    // for(prop in body) {
    //     bind.push(prop)
    // }

    let sql = `INSERT INTO security_login_basics SET ?`;
    // console.log(bind);
    database.query(sql, bind, function (err, lastId) {
      if (err) throw err;
      callback(lastId);
    });
  },

  login: function (username, password, callback) {
    this.find(username, function (user) {
      if (user) {
        if (bcrypt.compareSync(password, user.password)) {
          callback(user);
          return;
        }
      }
      callback(null);
    });
  },
};

module.exports = User;
