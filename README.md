# Repo contenant des petites applications et des petits composants de travail dans le développement Web

# 1. API Ajax Star Wars
   
 Le fonctionnement basique de la mécanique des APIs, aussi bien pour de la récupération de données en local que sur un lien externe

   **Ressources :**

 - Traversy Media - AJAX Crash Course (Vanilla JavaScript)
    https://www.youtube.com/watch?v=82hnvUYY6QA

 - The Coding Train - 10.4) Loading JSON data from a URL (Asynchronous Callbacks!) - p5.js Tutorial
    https://www.youtube.com/watch?v=6mT3r8Qn1VY

# 2. Password generator

 Un générateur de mots de passe avec plusieurs options de génération (majuscules, minuscules, chiffres, caractères spéciaux)

   **Ressources :**
    
 - Traversy Media - JavaScript Password Generator
    https://www.youtube.com/watch?v=duNmhKgtcsI

 - Florin Pop - 100 Days 100 Projects Challenge (projet n°13)
    Liste des 100 projets : https://www.florin-pop.com/blog/2019/09/100-days-100-projects
    Code source du générateur : https://codepen.io/FlorinPop17/pen/BaBePej
