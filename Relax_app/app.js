const container = document.querySelector('.container');
const text = document.querySelector('#text');

const totalTime = 7500;
const breatheTime = (totalTime / 5) * 2;
const holdTime = totalTime / 5;

breatheAnimation()

function breatheAnimation () {
    text.innerHTML = "Inspirez !"
    container.className = "container grow"
    console.log("1")

    setTimeout(() => {
        text.innerText = "Tenez"
        console.log("2")

        setTimeout(() => {
            text.innerText = "Expirez..."
            container.className = "container shrink"
            console.log("3")
        }, holdTime)

    }, breatheTime)
}

setInterval(breatheAnimation, totalTime)